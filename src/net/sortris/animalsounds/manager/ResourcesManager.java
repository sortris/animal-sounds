package net.sortris.animalsounds.manager;

import java.io.IOException;

import net.sortris.animalsounds.GameActivity;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

public class ResourcesManager {

	private static final ResourcesManager INSTANCE = new ResourcesManager();

	public Font font;
	
	public ITextureRegion menu_background_region;
	public ITextureRegion sound_on_region;
	public ITextureRegion sound_off_region;
	public ITextureRegion play_region;
	public ITextureRegion options_region;
	private BuildableBitmapTextureAtlas menuTextureAtlas;

	// TODO make getters and setters
	// TODO implement hermetization 
	public BitmapTextureAtlas splashTextureAtlas;
	public ITextureRegion splashTextureRegion;

	public Engine engine;
	public GameActivity activity;
	public Camera camera;
	public VertexBufferObjectManager vbom;

	// Game Texture
	public BuildableBitmapTextureAtlas gameTextureAtlas;

	// Game Texture Regions
	public ITextureRegion music_region;

	public ITextureRegion lvl1_background_region;
	public ITextureRegion lvl1_cat_region;
	public ITextureRegion lvl1_dog_region;
	public ITextureRegion lvl1_frog_region;
	public ITextureRegion lvl1_owl_region;

	public ITextureRegion lvl2_background_region;
	public ITextureRegion lvl2_lion_region;
	public ITextureRegion lvl2_elephant_region;
	public ITextureRegion lvl2_giraffe_region;

	public Music music;
	
	public Sound catSound;
	public Sound dogSound;
	public Sound frogSound;
	public Sound owlSound;

	public Sound lionSound;
	public Sound giraffeSound;
	public Sound elephantSound;

	public void loadMenuResources() {
		loadMenuGraphics();
		loadMenuAudio();
		loadMenuFonts();
	}

	public void loadGameResources() {
		loadGameGraphics();
		loadGameFonts();
		loadGameAudio();
	}

	private void loadMenuGraphics() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "background.png");
		sound_off_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "soundOff.png");
		sound_on_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "soundOn.png");
		play_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "play.png");
		options_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "options.png");
		
		try {
			this.menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			this.menuTextureAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	private void loadMenuFonts() {
		FontFactory.setAssetBasePath("font/");
		font = FontFactory.createFromAsset(activity.getFontManager(), activity.getTextureManager(), 256, 256, activity.getAssets(),
			    "crayon.ttf", 60, true, android.graphics.Color.BLACK);
		font.load();
	}

	public void unloadGameTextures() {
		// TODO (Since we did not create any textures for game scene yet)
	}

	private void loadMenuAudio() {
		MusicFactory.setAssetBasePath("audio/");
		try {
			this.music = MusicFactory.createMusicFromAsset(this.engine.getMusicManager(), activity, "music.mp3");
			this.music.setVolume(0.1f);
			this.music.setLooping(true);
		} catch (final IOException e) {
			Debug.e(e);
		}
	}

	private void loadGameGraphics() {

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
		gameTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);

		lvl1_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl1/lvl1Background.png");

		lvl1_cat_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl1/cat.png");
		lvl1_dog_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl1/dog.png");
		lvl1_frog_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl1/frog.png");
		lvl1_owl_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl1/owl.png");

		lvl2_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl2/lvl2Background.png");
		lvl2_giraffe_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl2/giraffe.png");
		lvl2_elephant_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl2/elephant.png");
		lvl2_lion_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameTextureAtlas, activity, "lvl2/lion.png");

		try {
			this.gameTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
			this.gameTextureAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	private void loadGameFonts() {

	}

	private void loadGameAudio() {
		SoundFactory.setAssetBasePath("audio/");
		try {
			this.catSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "cat.mp3");
			this.dogSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "dog.wav");
			this.frogSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "frog.mp3");
			this.owlSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "owl.wav");

			this.elephantSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "elephant.wav");
			this.lionSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "lion.wav");
			this.giraffeSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), activity, "giraffe.wav");
		} catch (final IOException e) {
			Debug.e(e);
		}
	}

	public void unloadMenuTextures() {
		menuTextureAtlas.unload();
	}

	public void loadMenuTextures() {
		menuTextureAtlas.load();
	}

	public void loadSplashScreen() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 512, 512, TextureOptions.REPEATING_BILINEAR);
		splashTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splashscreen.png", 0, 0);
		splashTextureAtlas.load();
	}

	public void unloadSplashScreen() {
		splashTextureAtlas.unload();
		splashTextureRegion = null;
	}

	/**
	 * @param engine
	 * @param activity
	 * @param camera
	 * @param vbom
	 * <br>
	 * <br>
	 *            We use this method at beginning of game loading, to prepare
	 *            Resources Manager properly, setting all needed parameters, so
	 *            we can latter access them from different classes (eg. scenes)
	 */
	public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom) {
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().camera = camera;
		getInstance().vbom = vbom;
	}

	public static ResourcesManager getInstance() {
		return INSTANCE;
	}

}
