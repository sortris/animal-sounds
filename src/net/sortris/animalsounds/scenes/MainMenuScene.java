package net.sortris.animalsounds.scenes;

import net.sortris.animalsounds.manager.SceneManager;
import net.sortris.animalsounds.manager.SceneManager.SceneType;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

public class MainMenuScene extends BaseScene implements IOnMenuItemClickListener {

	private MenuScene menuChildScene;
	private final int MENU_PLAY = 0;
	private final int MENU_OPTIONS = 1;
	private final int MENU_SOUND = 2;
	
	private IMenuItem soundMenuItem;
	
	private boolean soundPlayed = true;

	@Override
	public void createScene() {
		createBackground();
		createMenuChildScene();
		startSound();
	}

	private void startSound() {
		resourcesManager.music.play();
		soundPlayed = true;
	}
	
	private void stopSound() {
		resourcesManager.music.pause();
		soundPlayed = false;
	}

	@Override
	public void onBackKeyPressed() {
		System.exit(0);
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_MENU;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub

	}

	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
		switch (pMenuItem.getID()) {
		case MENU_SOUND:
			if (soundPlayed) {
				System.out.println("TUte");
				stopSound();
			}
			else {
				System.out.println("TU");
				startSound();
			}
			return true;
		case MENU_PLAY:
			SceneManager.getInstance().loadGameScene(engine, 1);
			return true;
		case MENU_OPTIONS:
			return true;
		default:
			return false;
		}
	}

	private void createBackground() {
		attachChild(new Sprite(0, 0, resourcesManager.menu_background_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});
	}

	private void createMenuChildScene() {
		menuChildScene = new MenuScene(camera);
		menuChildScene.setPosition(0, 0);

		final IMenuItem playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_PLAY, resourcesManager.play_region, vbom), 1.1f, 1);
		final IMenuItem optionsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_OPTIONS, resourcesManager.options_region, vbom), 1.1f, 1);
		soundMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_SOUND, resourcesManager.sound_on_region, vbom), 1.1f, 1);
		
		menuChildScene.addMenuItem(playMenuItem);
		menuChildScene.addMenuItem(optionsMenuItem);
		menuChildScene.addMenuItem(soundMenuItem);

		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);

		playMenuItem.setPosition(playMenuItem.getX(), playMenuItem.getY() + 50);
		optionsMenuItem.setPosition(optionsMenuItem.getX(), optionsMenuItem.getY() + 200);
		soundMenuItem.setPosition(soundMenuItem.getX()+350, soundMenuItem.getY()+120);
		
		menuChildScene.setOnMenuItemClickListener(this);

		setChildScene(menuChildScene);
	}

}
