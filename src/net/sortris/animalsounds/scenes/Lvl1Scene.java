package net.sortris.animalsounds.scenes;

import static net.sortris.animalsounds.Global.CAMERA_WIDTH;
import static net.sortris.animalsounds.Global.SCORE_POINTS;
import net.sortris.animalsounds.manager.SceneManager;
import net.sortris.animalsounds.manager.SceneManager.SceneType;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.HorizontalAlign;

public class Lvl1Scene extends BaseScene {

	private HUD gameHUD;

	private Text scoreText;

	private void addToScore(int i) {
		SCORE_POINTS += i;
		scoreText.setText("Score: " + SCORE_POINTS);
		
		if (SCORE_POINTS == 5) {
			stopSounds();
			SceneManager.getInstance().loadGameScene(engine, 2);
		}
	}

	private void stopSounds() {
		resourcesManager.catSound.pause();
		resourcesManager.dogSound.pause();
		resourcesManager.frogSound.pause();
		resourcesManager.owlSound.pause();
	}

	private void createHUD() {
		gameHUD = new HUD();

		// CREATE SCORE TEXT
		scoreText = new Text(0, 0, resourcesManager.font, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setText("Score: 0");
		scoreText.setPosition(CAMERA_WIDTH-scoreText.getWidth()-30, 0);
		gameHUD.attachChild(scoreText);

		camera.setHUD(gameHUD);
	}

	@Override
	public void createScene() {
		createBackground();
		createAnimals();
		createHUD();
	}

	private void createAnimals() {

		int horizonHeight = 350;
		int margin = 50;

		Sprite catSprite = new Sprite(margin, horizonHeight, resourcesManager.lvl1_cat_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.catSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		
		
		Sprite dogSprite = new Sprite(catSprite.getWidth() + margin * 2, horizonHeight, resourcesManager.lvl1_dog_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.dogSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		

		Sprite frogSprite = new Sprite(catSprite.getWidth() + dogSprite.getWidth() + margin * 3, horizonHeight, resourcesManager.lvl1_frog_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.frogSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		

		Sprite owlSprite = new Sprite(catSprite.getWidth() + dogSprite.getWidth() + frogSprite.getWidth() + margin * 4, horizonHeight,
				resourcesManager.lvl1_owl_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.owlSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		
		
		registerTouchArea(catSprite);
		registerTouchArea(dogSprite);
		registerTouchArea(frogSprite);
		registerTouchArea(owlSprite);
		
		attachChild(catSprite);
		attachChild(dogSprite);
		attachChild(frogSprite);
		attachChild(owlSprite);
	}

	@Override
	public void onBackKeyPressed() {
		stopSounds();
		SceneManager.getInstance().loadMenuScene(engine);
		SCORE_POINTS = 0;
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		camera.setHUD(null);
		
		// TODO code responsible for disposing scene
		// removing all game scene objects.
	}

	private void createBackground() {
		attachChild(new Sprite(0, 0, resourcesManager.lvl1_background_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});
	}
	
	
}
