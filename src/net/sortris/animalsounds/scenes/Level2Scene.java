package net.sortris.animalsounds.scenes;

import static net.sortris.animalsounds.Global.CAMERA_WIDTH;
import static net.sortris.animalsounds.Global.SCORE_POINTS;
import net.sortris.animalsounds.manager.SceneManager;
import net.sortris.animalsounds.manager.SceneManager.SceneType;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.HorizontalAlign;

public class Level2Scene extends BaseScene {

	private HUD gameHUD;

	private Text scoreText;

	private void addToScore(int i) {
		SCORE_POINTS += i;
		scoreText.setText("Score: " + SCORE_POINTS);
	}

	private void createHUD() {
		gameHUD = new HUD();

		// CREATE SCORE TEXT
		scoreText = new Text(0, 0, resourcesManager.font, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setText("Score: "+SCORE_POINTS);
		scoreText.setPosition(CAMERA_WIDTH-scoreText.getWidth()-30, 0);
		gameHUD.attachChild(scoreText);

		camera.setHUD(gameHUD);
	}

	private void stopSounds() {
		resourcesManager.giraffeSound.pause();
		resourcesManager.lionSound.pause();
		resourcesManager.elephantSound.pause();
	}
	
	@Override
	public void createScene() {
		createBackground();
		createAnimals();
		createHUD();
	}

	private void createAnimals() {

		int horizonHeight = 320;
		int margin = 90;

		Sprite elephantSprite = new Sprite(margin, horizonHeight, resourcesManager.lvl2_elephant_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.elephantSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		
		
		Sprite giraffeSprite = new Sprite(elephantSprite.getWidth() + margin * 2, horizonHeight, resourcesManager.lvl2_giraffe_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.giraffeSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};
		

		Sprite lionSprite = new Sprite(elephantSprite.getWidth() + giraffeSprite.getWidth() + margin * 3, horizonHeight, resourcesManager.lvl2_lion_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
			
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					this.setScale(1.1f);
					stopSounds();
					resourcesManager.lionSound.play();
					addToScore(1);
				}
				if (pSceneTouchEvent.isActionUp()) {
					this.setScale(1f);
				}
				return true;
			}
		};

		registerTouchArea(elephantSprite);
		registerTouchArea(giraffeSprite);
		registerTouchArea(lionSprite);
		
		attachChild(elephantSprite);
		attachChild(giraffeSprite);
		attachChild(lionSprite);
	}

	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().loadMenuScene(engine);
		SCORE_POINTS = 0;
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		camera.setHUD(null);
	}

	private void createBackground() {
		attachChild(new Sprite(0, 0, resourcesManager.lvl2_background_region, vbom) {
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) {
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});
	}
}
