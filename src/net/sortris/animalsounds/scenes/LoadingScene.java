package net.sortris.animalsounds.scenes;

import static net.sortris.animalsounds.Global.CAMERA_HEIGHT;
import static net.sortris.animalsounds.Global.CAMERA_WIDTH;
import net.sortris.animalsounds.manager.SceneManager.SceneType;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

public class LoadingScene extends BaseScene {
	
	private Text text;

	@Override
	public void createScene() {
		setBackground(new Background(Color.WHITE));
		text = new Text(0, 0, resourcesManager.font, "Loading...", vbom);
		attachChild(text);
		text.setPosition(CAMERA_WIDTH / 2 - (text.getWidth() / 2), CAMERA_HEIGHT / 2 - (text.getHeight() / 2));
	}

	@Override
	public void onBackKeyPressed() {
		return;
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_LOADING;
	}

	@Override
	public void disposeScene() {

	}
}
